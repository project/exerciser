<?php

/**
 * @file
 * Contains exerciser.drush.inc.
 */

/**
 * Implements hook_drush_command().
 */
function exerciser_drush_command() {

  $commands['exerciser'] = [
    'description' => 'Runs a test on all nodes of a specified content type',
    'aliases' => ['ex'],
    'options' => [
      'host' => 'Host',
      'curl' => 'Use curl for connections',
      'curl_timeout' => 'Curl timeout in seconds.',
      'db' => 'Debug',
    ],
    'examples' => [
      'drush x1' => 'exerciser',
      'drush x2' => 'exerciser --host=http://yoursite.com',
      'drush x3' => 'exerciser --curl',
      'drush x4' => 'exerciser --curl --curl_timeout=2',
    ],
  ];

  $commands['exerciser-restart'] = [
    'description' => 'Restarts an exerciser run on all nodes of a specified content type.',
    'aliases' => ['exr'],
    'options' => [
      'host' => 'Host',
      'curl' => 'Use curl for connections',
      'curl_timeout' => 'Curl timeout in seconds.',
      'db' => 'Debug',
    ],
    'examples' => [
      'drush x1' => 'exerciser-restart',
      'drush x2' => 'exerciser-restart --host=http://yoursite.com',
      'drush x3' => 'exerciser-restart --curl',
      'drush x4' => 'exerciser-restart --curl --curl_timeout=2',
    ],
  ];

  return $commands;
}

/**
 * Implements exerciser (ex) command.
 */
function drush_exerciser() {

  global $base_url;

  if (str_contains($base_url, '//default') && empty(drush_get_option('host', ''))) {
    print "The base_url global variable is not set for this web site.\r\n";
    print "It is set to the default url: $base_url.\r\n";
    print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
    print "Add something like this to your drush commands:\r\n";
    print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
    print "\r\n";
    print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
    print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
    return;
  }

  $content_type = exerciser_drush_choose_content_type('what content type');
  if ($content_type === FALSE) {
    print "\r\nExerciser - Canceled\r\n";
    return;
  }

  $run_timestamp = time();

  \Drupal::logger('marker')->notice('MARKER - Start Exerciser Run for - ' . $content_type . ' - ' . date("Y/m/d H:i:s", $run_timestamp));

  $host = drush_get_option('host', $base_url);
  $curl = drush_get_option('curl', FALSE);
  $curl_timeout_raw = drush_get_option('curl_timeout', 0);
  $curl_timeout = (isset($curl_timeout_raw) && $curl_timeout_raw != 0 ? $curl_timeout_raw : 2);
  $db = drush_get_option('db', FALSE);

  if ($nid == NULL) {
    exerciser_emit_divider_line();
    exerciser_emit_header_line();
    exerciser_emit_divider_line();

    $node_count = 1;
    $connection = \Drupal::database();
    $query = $connection->query("SELECT `nid`, `type` FROM {node_field_data} WHERE `status` = 1 AND `type` = :type ORDER BY `nid` ASC",
                                [':type' => $content_type]);
    $results = $query->fetchAll();
    foreach ($results as $row) {
      $fields_array = workout_test_node($row->nid, $row->type, $host, $curl, $curl_timeout, $db, $run_timestamp);
      $fields_array['id'] = $node_count;
      exerciser_emit_data_line($fields_array);
      $node_count++;
    }
    exerciser_emit_divider_line();

  }

  print "\r\n\r\nCompleted - drush exerciser.\r\n\r\n";
  \Drupal::logger('marker')->notice('MARKER - End Exerciser Run for - ' . $content_type . ' - ' . date("Y/m/d H:i:s", $run_timestamp));

}

/**
 * Implements exerciser-restart (exr) command.
 */
function drush_exerciser_restart() {

  global $base_url;

  if (str_contains($base_url, '//default') && empty(drush_get_option('host', ''))) {
    print "The base_url global variable is not set for this web site.\r\n";
    print "It is set to the default url: $base_url.\r\n";
    print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
    print "Add something like this to your drush commands:\r\n";
    print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
    print "\r\n";
    print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
    print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
    return;
  }

  $content_type = exerciser_drush_choose_content_type('what content type');
  if ($content_type === FALSE) {
    print "\r\nExerciser - Canceled\r\n";
    return;
  }

  $run_timestamp = _drush_choose_run('To Restart');

  $host = drush_get_option('host', $base_url);
  $curl = drush_get_option('curl', FALSE);
  $curl_timeout = 2;
  $curl_timeout_raw = drush_get_option('curl_timeout', 0);
  $curl_timeout = (isset($curl_timeout_raw) && $curl_timeout_raw != 0 ? $curl_timeout_raw : 2);
  $db = drush_get_option('db', FALSE);

  if ($run_timestamp == 0) {
    print "\r\n\r\n Canceled - drush workout-restart $nid.\r\n\r\n";
    return;
  }

  $connection = \Drupal::database();

  if ($nid == NULL) {
    // Find the max(nid) for the run and start there.
    $nid = $connection->query("SELECT MAX(`nid`) FROM {workout} WHERE `run` = :run",
                              [
                                ':run' => $run_timestamp,

                              ])->fetchField();

  }

  if (is_numeric($nid)) {

    exerciser_emit_divider_line();
    exerciser_emit_header_line();
    exerciser_emit_divider_line();

    $query = $connection->query("SELECT `nid`, `type` FROM {node_field_data}  WHERE  `status` = 1  AND `type` = :type AND `nid` >= :nid ORDER BY `nid` ASC",
      [
        ':nid' => $nid,
        ':type' => $content_type,
      ]);
    $results = $query->fetchAll();
    foreach ($results as $row) {
      exerciser_emit_data_line(workout_test_node($row->nid, $row->type, $host, $curl, $curl_timeout, $db, $run_timestamp));

    }

    exerciser_emit_divider_line();

  }

  print "\r\n\r\n Completed - drush exerciser-restart $nid.\r\n\r\n";
  \Drupal::logger('marker')->notice('MARKER - End Exerciser Run for - ' . $content_type . ' - ' . date("Y/m/d H:i:s", $run_timestamp));

}

/**
 * A choose_content_type helper.
 */
function exerciser_drush_choose_content_type($why = '') {
  $options_arr = [];
  $option_number = 1;
  $connection = \Drupal::database();
  $query = $connection->query("SELECT DISTINCT `type` FROM {node} ORDER BY `type` ASC");
  $results = $query->fetchAll();
  foreach ($results as $row) {
    $options_arr[$option_number++] = $row->type;
  }
  $choice = drush_choice($options_arr, "Choose content_type $why.");
  if ($choice == 0) {
    return FALSE;
  }
  else {
    return $options_arr[$choice];
  }
}

/**
 * Displays Divider Line.
 */
function exerciser_emit_divider_line() {
  print '+' . str_pad('-', 211, '-') . "+\r\n";
}

/**
 * Displays Header Line.
 */
function exerciser_emit_header_line() {

  // TEXT output.
  print '| ' .
    str_pad('Id', 6) . '| ' .
    str_pad('Run Date', 20) . '| ' .
    str_pad('Nid', 15) . '| ' .
    str_pad('Content Type', 15) . '| ' .
    str_pad('Status', 6) . '| ' .
    str_pad('Size', 8) . '| ' .
    str_pad('Wid Before', 12) . '| ' .
    str_pad('Wid After', 12) . '| ' .
    str_pad('∆ wid ', 6) . '| ' .
    str_pad('∆ php ', 6) . '| ' .
    str_pad('∆mrgnc', 6) . '| ' .
    str_pad('∆alert', 6) . '| ' .
    str_pad('∆crtcl', 6) . '| ' .
    str_pad('∆error', 6) . '| ' .
    str_pad('∆warn ', 6) . '| ' .
    str_pad('∆notic', 6) . '| ' .
    str_pad('∆ info', 6) . '| ' .
    str_pad('∆debug', 6) . '| ' .
    str_pad('Time Stamp', 20) . "|\r\n";

}

/**
 * Displays Data Line.
 */
function exerciser_emit_data_line($field_array) {

  print '| ' .
    str_pad($field_array['id'], 6) . '| ' .
    str_pad(date('Y/m/d H:i:s', $field_array['run']), 20) . '| ' .
    str_pad($field_array['nid'], 15) . '| ' .
    str_pad(substr($row->type, 0, 14), 15) . '| ' .
    str_pad($field_array['html_status'], 6) . '| ' .
    str_pad($field_array['content_size'], 8) . '| ' .
    str_pad($field_array['wid_before_test'], 12) . '| ' .
    str_pad($field_array['wid_after_test'], 12) . '| ' .
    str_pad($field_array['delta_watchdog'], 6) . '| ' .
    str_pad($field_array['delta_php'], 6) . '| ' .
    str_pad($field_array['delta_emergency'], 6) . '| ' .
    str_pad($field_array['delta_alert'], 6) . '| ' .
    str_pad($field_array['delta_critical'], 6) . '| ' .
    str_pad($field_array['delta_error'], 6) . '| ' .
    str_pad($field_array['delta_warning'], 6) . '| ' .
    str_pad($field_array['delta_notice'], 6) . '| ' .
    str_pad($field_array['delta_info'], 6) . '| ' .
    str_pad($field_array['delta_debug'], 6) . '| ' .
    str_pad(date('Y/m/d H:i:s', $field_array['timestamp']), 20) . "|\r\n";

}

/**
 * Choose Run.
 */
