CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Operation
 * Troubleshooting
 * FAQ
 * Maintainers
 * Appendix -  Drush Commands



INTRODUCTION
------------
Some php errors occur every time a page is loaded and renderded.
Exerciser can be useful for forcing these errors to be listed in Drupals Log.
Once they are listed in Drupals Log you can begin to address and repair the underlying issues. A tool like Grouper can be used to group similar individual log messages together and count the number of number of log messages related to the issue.

Exerciser adds the ability to load pages by content type to the Workout Module.
Exerciser loads every published page of a chosen content type on a site in an attempt to generate errors.
This can be useful for catching any module or theme errors that occur while rendering pages.

You can use Exerciser to determine if there are any errors while loading and rendering pages. Exerciser will be of no help finding errors that occur when users click buttons or perform actions on pages.

Exerciser allows you to test smaller subsets of pages on your site.
This saves time compared to a complete Workout run which loads every page.
Can be useful for working on issues for a single or small set of content types.

The results of Exerciser processing runs are logged.
Exerciser allows you to easily compare the results of different runs.


The following information is collected for each page that gets loaded.
  Date            The date and time the test was performed.
  Run Date        The date and time the run started.
  Nid             The node id of the node page being tested.
  Content Type    The Content Type of the node page being tested.
  Status          The html request status code for the request. 200 is perfect.
  Size            The number of bytes returned by the request. The size of the page.
  Wid Before      The last log wid (id) in the log before the test was run.
  Wid After       The last log wid (id) in the log after the test was run.
  ∆ wid           The number of log messages as a result of running the test. Should be 0.
  ∆ php           The number of php messages as a result of running the test.
  ∆ mrgnc         The number of php Emergency messages as a result of running the test.
  ∆ alert         The number of php Alert messages as a result of running the test.
  ∆ crtcl         The number of php Critical messages as a result of running the test.
  ∆ error         The number of php Error messages as a result of running the test.
  ∆ warn          The number of php Warning messages as a result of running the test.
  ∆ notice        The number of php Notice messages as a result of running the test.
  ∆ info          The number of php Info messages as a result of running the test.
  ∆ debug         The number of php Debug messages as a result of running the test.




REQUIREMENTS
------------
Drupal 8 or Drupal 9
Drupal Exerciser Module



OPTIONAL
--------
Drush 8
Drush 9



INSTALLATION
------------
Install like any other Drupal Module.



CONFIGURATION
-------------
There is no configuration for the Exerciser module.


OPERATION
---------
Go to the Admin Reports page and click Exerciser.
Use the Runs tab to create new Runs.
Choose what you want to do with the results when the testing process completes.
Select the Content Types to Test.
Click Go.

The other tabs are for displaying the Run Results.


__________________________________________________________________________________________
Runs

Admin | Reports | Exerciser
/admin/reports/exerciser/manage
Create new Runs or Delete old Runs from the Runs Page.

__________________________________________________________________________________________
Summary

Admin | Reports | Exerciser | Summary
/admin/reports/exerciser/summary
Displays a listings of all Runs showing only pages which had errors or messages.

If no errors were detected during any of the tests there will be No Data available.
You can click Full Summary to see all the raw data from the tests.
__________________________________________________________________________________________
Full Summary

Admin | Reports | Exerciser | Full Summary
/admin/reports/exerciser/summary-full
Displays a listings of all Runs showing all pages.
__________________________________________________________________________________________
CSV Summary

Admin | Reports | Exerciser  | CSV Summary
/admin/reports/exerciser/summary-csv
Download a csv listings of all Runs showing all pages.
__________________________________________________________________________________________
Distribution

Admin | Reports | Exerciser  | Distribution
/admin/reports/exerciser/distribution
Displays a list of all content types and how many pages for each content type.

__________________________________________________________________________________________


TROUBLESHOOTING
---------------
Make sure the Exerciser module is enabled.
Go to Admin | Reports | Workout
/admin/reports/exerciser/manage


If there is a failure in the middle of a run.
You can use the drush exerciser-restart command
to restart an incomplete exerciser run.


Make sure your catching is turned off:
go to /admin/config/development/performance
Make sure cacheing is set to <no caching>
In order to catch errors when page is rendering.



FOR DRUSH ONLY

While running the workout or exerciser drush commands you may get this message.

The base_url global variable is not set for this web site.
It is set to the default url: http://default
You will need to feed in your host name as an option into exerciser or workout drush commands.
Add something like this to your drush commands:
--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-

Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.

For Drush to know the URL of your local website, you can add the following lines to your drush.local.yml file.

# File: sites/default/drush.local.yml
options:
  uri: "http://your.local.site/"

You will also need to make sure that the above drush config file is actually picked up by Drush. You can do this as follows:

# File: drush/drush.yml
drush:
  paths:
    config:
      - "web/sites/default/drush.local.yml"


# run to clear your cache
drush cr

# then run to view your sites url.
drush get-base-url

running drush uli should also now return a link to your local site instead of http://default....



FAQ
---

Q: Can I View the results of more than one Run?

A: Yes, can view the results of several runs simultaneously.

Q: How can I tell the difference between the runs?

A: The results for the same node are listed together.
   On A Summary Page the results for a node are Zebra Striped.
   Several lines forming a node group will be alternately shaded or not.
   In the drush commands there are dividing lines between the node groups.
   In a CSV file there will be a blank row between each group of nodes

Q: Can I use curl to run the tests?

Q: Can I perform the tests on another server?

A: Yes, in the Web interface add a Query String to the Runs page.
   add an option ?--host=http://the-domain-you-want-to-test
   Refresh the Runs page before starting the runoff the option to take.
   In the drush interface add a --host option to the drush command.
   workout --host=http://the-domain-you-want-to-test

Q: Can I use curl to run the tests?

A: Yes, in the Web interface add a Query String to the Runs page.
   add an option ?curl=1
   Refresh the Runs page before starting the runoff the option to take.
   In the drush interface add a --curl option to the drush command.
   exerciser --curl_timeout=2

Q: Can I set the curl timeout for the tests?

A: Yes, in the Web interface add a Query String to the Runs page.
   add an option ?curl=1&curl_timeout=5
   Refresh the Runs page before starting the runoff the option to take.
   In the drush interface add a --curl_timeout=5 option go the drush command.
   exerciser --curl --curl_timeout=2



MAINTAINERS
-----------

Current maintainer:

 * Seth Snyder (tfa) - https://www.drupal.org/u/seth-snyder


APPENDIX - Drush Commands
-------------------------

__________________________________________________________________________________________
DRUSH COMMAND LINE INTERFACE
__________________________________________________________________________________________

exerciser
Runs a test on all nodes of a specified content type.
You will be prompted to choose which content type to test.

Examples:
 drush x1                                  exerciser                            
 drush x2                                  exerciser --host=http://yoursite.com 
 drush x3                                  exerciser --curl                     
 drush x4                                  exerciser --curl --curl_timeout=2


Options:
 --curl                                    Use curl for connections 
 --curl_timeout                            Curl timeout in seconds. 
 --db                                      Debug                    
 --host                                    Host

Aliases: ex



__________________________________________________________________________________________

exerciser-restart
Restarts an exerciser on all nodes of a specified content type.
You will be prompted to choose which content type to test.
You will be prompted to choose which run to restart.
The run wil resume where it ended previously.

Examples:
 drush x1                                  exerciser-restart                            
 drush x2                                  exerciser-restart --host=http://yoursite.com 
 drush x3                                  exerciser-restart --curl                     
 drush x4                                  exerciser-restart --curl --curl_timeout=2

Options:
 --curl                                    Use curl for connections 
 --curl_timeout                            Curl timeout in seconds. 
 --db                                      Debug                    
 --host                                    Host

Aliases: exr

__________________________________________________________________________________________



