<?php

namespace Drupal\exerciser\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form with examples on how to use cache.
 */
class ExerciserForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'exerciser_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Loads every page of the selected content types to determine if any errors are produced. Choose one or more content types and click the Go button to start the test.'),
    ];

    $form['batch'] = [
      '#type' => 'select',
      '#title' => 'Choose Results',
      '#options' => [
        'exerciser.summary' => $this->t('Run and Display Summary'),
        'exerciser.summary_full' => $this->t('Run and Display Full Summary.'),
        'exerciser.summary_csv' => $this->t('Run and Download CSV Summary.'),
        'none' => $this->t('Run Only.'),
      ],
    ];

    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    $content_types_array = array_keys($types);

    $content_types_options_array = [];
    foreach ($content_types_array as $content_type) {
      $content_types_options_array[$content_type] = $content_type;
    }

    // CheckBoxes.
    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#options' => $content_types_options_array,
      '#title' => $this->t('Select Content Types to Test?'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Go',
      '#name' => 'new_run',
      '#prefix' => '&nbsp;<br />',
      '#suffix' => '<br />&nbsp;<br />&nbsp;',
    ];

    $form['managementdescription'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Select one or more previous runs to remove and click Delete Selected Runs.'),
    ];

    // TableSelect.
    $header = [
      'run_date' => $this->t('Run Date'),
    ];

    $form['table'] = [
      '#type' => 'tableselect',
      '#title' => $this->t('Users'),
      '#header' => $header,
      '#options' => workout_get_runs_array(),
      '#empty' => $this->t('No Runs found'),
    ];

    $form['delete_runs'] = [
      '#type' => 'submit',
      '#value' => 'Delete Selected Runs',
      '#name' => 'delete_runs',
      '#prefix' => '&nbsp;<br />',
      '#suffix' => '<br />&nbsp;<br />&nbsp;',
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Find Out Which submit button was clicked.
    $triggering_element = $form_state->getTriggeringElement();
    if (is_array($triggering_element) && isset($triggering_element) && isset($triggering_element['#name'])) {
      if ($triggering_element['#name'] == 'new_run') {

        global $base_url;
        $host = (isset($_GET['host']) ? $_GET['host'] : $base_url);
        $curl = (isset($_GET['curl']) ? $_GET['curl'] : FALSE);
        $curl_timeout = (isset($_GET['curl_timeout']) ? $_GET['curl_timeout'] : 2);
        $db = (isset($_GET['db']) ? $_GET['db'] : FALSE);

        // Build the list of content types for the query.
        $content_types_array = $form_state->getValue('content_types');
        $content_types = '';
        foreach ($content_types_array as $content_type_key => $content_type_value) {
          if (strcmp($content_type_key, $content_type_value) == 0) {

            if (empty($content_types)) {
              $content_types .= "'$content_type_value'";
            }
            else {
              $content_types .= ",'$content_type_value'";
            }

          }
        }

        $redirect_to_page = $form_state->getValue('batch');
        $tempstore = \Drupal::service('user.private_tempstore')->get('workout');
        $tempstore->set('destination', $redirect_to_page);

        // Set the batch, using convenience methods.
        $batch = [];
        $batch = $this->generateBatch($host, $curl, $curl_timeout, $db, $content_types);
        batch_set($batch);

      }
      elseif ($triggering_element['#name'] == 'delete_runs') {

        $run_options_array = $form_state->getValue('table');
        $runs_to_delete = '';
        foreach ($run_options_array as $run_ts_key => $run_ts_value) {
          if ($run_ts_key == $run_ts_value) {
            if (empty($runs_to_delete)) {
              $runs_to_delete .= $run_ts_value;
            }
            else {
              $runs_to_delete .= ",$run_ts_value";
            }
          }
        }

        if (empty($runs_to_delete)) {
          \Drupal::messenger()->addMessage('No Runs Were Selected.', MessengerInterface::TYPE_ERROR);
        }
        else {
          workout_delete_runs($runs_to_delete);
        }

      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public function generateBatch($host, $curl, $curl_timeout, $db, $content_types) {
    $i = 0;
    $operations = [];
    $run_timestamp = time();
    $connection = \Drupal::database();
    \Drupal::logger('marker')->notice('MARKER - Start Exerciser Run - ' . date("Y/m/d H:i:s", $run_timestamp));

    $num_operations = $connection->query("SELECT count(`nid`) FROM {node_field_data} WHERE `status` = 1 AND `type` IN ($content_types)")->fetchField();

    $query = $connection->query("SELECT `nid`, `type` FROM {node_field_data} WHERE `status` = 1 AND `type` IN ($content_types) ORDER BY `nid` ASC");

    $results = $query->fetchAll();
    foreach ($results as $row) {
      $i++;
      $operations[] = [
        'workout_test_node_batch_operation',
         [
           $row->nid,
           $row->type,
           $host,
           $curl,
           $curl_timeout,
           $db,
           $run_timestamp,
           $this->t('(Operation @operation)', ['@operation' => $i]),
         ],
      ];

    }

    $batch = [
      'title' => $this->t('Creating an array of @num operations', ['@num' => $num_operations]),
      'operations' => $operations,
      'finished' => 'workout_test_node_batch_finished',
    ];
    return $batch;
  }

}
