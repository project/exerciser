<?php

namespace Drupal\exerciser\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class ExerciserCommands extends DrushCommands {

  /**
   * Runs a test on all nodes of a specified content type.
   *
   * @param array $options
   *   Command Options.
   *
   * @option host
   *   Setting the --host option runs the tests on a different system.
   * @option curl
   *   Setting the --curl option uses curl to access your local system.
   * @option curl_timeout
   *   Curl timeout in seconds.
   * @option db
   *   Debug.
   * @usage drush exerciser
   *   - Runs a test on all nodes of a specified content type.
   *
   * @command exerciser:exerciser
   * @aliases exerciser ex
   */
  public function exerciser(array $options = [
    'host' => NULL,
    'curl' => FALSE,
    'curl_timeout' => 2,
    'db' => FALSE,
  ]) {

    global $base_url;
    $host = (isset($options['host']) ? $options['host'] : '');
    if (str_contains($base_url, '//default') && empty($host)) {
      print "The base_url global variable is not set for this web site.\r\n";
      print "It is set to the default url: $base_url.\r\n";
      print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
      print "Add something like this to your drush commands:\r\n";
      print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
      print "\r\n";
      print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
      print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
      return;
    }

    $content_type = $this->chooseContentType('what content type');
    if ($content_type === FALSE) {
      print "\r\nExerciser - Canceled\r\n";
      return;
    }

    $run_timestamp = time();

    \Drupal::logger('marker')->notice('MARKER - Start Exerciser Run for - ' . $content_type . ' - ' . date("Y/m/d H:i:s", $run_timestamp));
    $host = (isset($options['host']) ? $options['host'] : $base_url);
    $curl = $options['curl'];
    $curl_timeout = (isset($options['curl_timeout']) ? $options['curl_timeout'] : 2);
    $db = $options['db'];

    if ($nid == NULL) {
      $this->emitDividerLine();
      $this->emitHeaderLine();
      $this->emitDividerLine();

      $node_count = 1;
      $connection = \Drupal::database();
      $query = $connection->query("SELECT `nid`, `type` FROM {node_field_data} WHERE `status` = 1 AND `type` = :type ORDER BY `nid` ASC",
                                  [':type' => $content_type]);
      $results = $query->fetchAll();
      foreach ($results as $row) {
        $fields_array = workout_test_node($row->nid, $row->type, $host, $curl, $curl_timeout, $db, $run_timestamp);
        $fields_array['id'] = $node_count;
        $this->emitDataLine($fields_array);
        $node_count++;
      }
      $this->emitDividerLine();

    }

    print "\r\n\r\nCompleted - drush exerciser.\r\n\r\n";
    \Drupal::logger('marker')->notice('MARKER - End Exerciser Run for - ' . $content_type . ' - ' . date("Y/m/d H:i:s", $run_timestamp));

    $this->logger()->success(dt('exerciser command function. ' . $content_types));

  }

  /**
   * Restarts an exerciser run on all nodes of a specified content type.
   *
   * @param array $options
   *   Command Options.
   *
   * @option host
   *   Setting the --host option runs the tests on a different system.
   * @option curl
   *   Setting the --curl option uses curl to access your local system.
   * @option curl_timeout
   *   Curl timeout in seconds.
   * @option db
   *   Debug.
   * @usage drush exerciser-restart
   *   - Restarts an exerciser run on all nodes of a specified content type.
   *
   * @command exerciser:exerciserReStart
   * @aliases exerciser-restart exr
   */
  public function exerciserReStart(array $options = [
    'host' => NULL,
    'curl' => FALSE,
    'curl_timeout' => 2,
    'db' => FALSE,
  ]) {

    global $base_url;
    $host = (isset($options['host']) ? $options['host'] : '');
    if (str_contains($base_url, '//default') && empty($host)) {
      print "The base_url global variable is not set for this web site.\r\n";
      print "It is set to the default url: $base_url.\r\n";
      print "You will need to feed in your host name as an option into exerciser or workout drush commands.\r\n";
      print "Add something like this to your drush commands:\r\n";
      print "--host=http://your-sites-local-url OR --host=http://localhost OR --host=http://your-containers-ip-address:port\r\n";
      print "\r\n";
      print "Or you can fix the issue with drush by adding and enabling a sites/default/drush.local.yml file.\r\n";
      print "See the readme file or the drush documentation for more info on setting up your site domain for drush.\r\n";
      return;
    }

    $content_type = $this->chooseContentType('To Restart');
    if ($content_type === FALSE) {
      print "\r\nExerciser - Canceled\r\n";
      return;
    }

    $run_timestamp = $this->chooseRun('To Restart');

    $host = (isset($options['host']) ? $options['host'] : $base_url);
    $curl = $options['curl'];
    $curl_timeout = (isset($options['curl_timeout']) ? $options['curl_timeout'] : 2);
    $db = $options['db'];

    if ($run_timestamp == 0) {
      print "\r\n\r\n Canceled - drush workout-restart $nid.\r\n\r\n";
      return;
    }

    $connection = \Drupal::database();

    if ($nid == NULL) {
      // Find the max(nid) for the run and start there.
      $nid = $connection->query("SELECT MAX(`nid`) FROM {workout} WHERE `run` = :run",
                                [
                                  ':run' => $run_timestamp,

                                ])->fetchField();

    }

    if (is_numeric($nid)) {

      $this->emitDividerLine();
      $this->emitHeaderLine();
      $this->emitDividerLine();

      $query = $connection->query("SELECT `nid`, `type` FROM {node_field_data}  WHERE  `status` = 1  AND `type` = :type AND `nid` >= :nid ORDER BY `nid` ASC",
        [
          ':nid' => $nid,
          ':type' => $content_type,
        ]);
      $results = $query->fetchAll();
      foreach ($results as $row) {
        $this->emitDataLine(workout_test_node($row->nid, $row->type, $host, $curl, $curl_timeout, $db, $run_timestamp));

      }

      $this->emitDividerLine();

    }

    \Drupal::logger('marker')->notice('MARKER - End Exerciser Run for - ' . $content_type . ' - ' . date("Y/m/d H:i:s", $run_timestamp));

    $this->logger()->success("Completed - drush exerciser-restart - $content_type");
  }

  /**
   * Helper method to get a content type to process.
   */
  public function chooseContentType($why = '') {
    global $_exerciser_count;
    $cancel = [0 => 'CANCEL'];
    $content_types = array_keys(node_type_get_types());
    $choices = array_merge($cancel, $content_types);

    print "\r\n CONTENT TYPES\r\n\r\n";
    foreach ($choices as $key => $choice) {
      print ' ' . str_pad($key, 3) . ' -- ' . str_pad($choice, 32) . "\r\n";
    }

    $_exerciser_count = count($content_types);

    $choice = $this->io()->ask($why, 1, function ($number) {
      global $_exerciser_count;

      if (!is_numeric($number)) {
        throw new \RuntimeException('You must type a number.');
      }

      elseif ($number < 0 || $number > $_exerciser_count) {
        throw new \RuntimeException('Invalid Choice.');
      }

      return (int) $number;
    });

    if ($choice == 0) {
      print "Canceled";
      return FALSE;
    }

    return $choices[$choice];
  }

  /**
   * Helper method to get the runt to process.
   */
  public function chooseRun($why) {
    global $_exerciser_count2;
    $cancel = [0 => 'CANCEL'];
    $connection = \Drupal::database();
    $query = $connection->query("SELECT DISTINCT `run` FROM {workout} ORDER BY `run` ASC");
    $results = $query->fetchAll();

    $choices = array_merge($cancel, $results);

    print "\r\n RUNS\r\n\r\n";
    foreach ($choices as $key => $row) {
      if ($key == 0) {
        print ' ' . str_pad((string) $key, 3) . ' -- ' . str_pad('CANCEL', 32) . "\r\n";

      }
      else {
        print ' ' . str_pad((string) $key, 3) . ' -- ' . str_pad(date('Y/m/d H:i:s', $row->run), 32) . "\r\n";
      }
    }

    $_exerciser_count2 = count($choices);

    $choice = $this->io()->ask($why, 1, function ($number) {
      global $_exerciser_count2;

      if (!is_numeric($number)) {
        throw new \RuntimeException('You must type a number.');
      }

      elseif ($number < 0 || $number > $_exerciser_count2 - 1) {
        throw new \RuntimeException('Invalid Choice.');
      }

      return (int) $number;
    });

    if ($choice == 0) {
      return FALSE;
    }

    return $choices[$choice]->run;

  }

  /**
   * Displays Divider Line.
   */
  public function emitDividerLine() {
    print '+' . str_pad('-', 211, '-') . "+\r\n";
  }

  /**
   * Displays Header Line.
   */
  public function emitHeaderLine() {

    // TEXT output.
    print '| ' .
      str_pad('Id', 6) . '| ' .
      str_pad('Run Date', 20) . '| ' .
      str_pad('Nid', 15) . '| ' .
      str_pad('Content Type', 15) . '| ' .
      str_pad('Status', 6) . '| ' .
      str_pad('Size', 8) . '| ' .
      str_pad('Wid Before', 12) . '| ' .
      str_pad('Wid After', 12) . '| ' .
      str_pad('∆ wid ', 6) . '| ' .
      str_pad('∆ php ', 6) . '| ' .
      str_pad('∆mrgnc', 6) . '| ' .
      str_pad('∆alert', 6) . '| ' .
      str_pad('∆crtcl', 6) . '| ' .
      str_pad('∆error', 6) . '| ' .
      str_pad('∆warn ', 6) . '| ' .
      str_pad('∆notic', 6) . '| ' .
      str_pad('∆ info', 6) . '| ' .
      str_pad('∆debug', 6) . '| ' .
      str_pad('Time Stamp', 20) . "|\r\n";

  }

  /**
   * Displays Data Line.
   */
  public function emitDataLine($field_array) {

    print '| ' .
      str_pad($field_array['id'], 6) . '| ' .
      str_pad(date('Y/m/d H:i:s', $field_array['run']), 20) . '| ' .
      str_pad($field_array['nid'], 15) . '| ' .
      str_pad(substr($row->type, 0, 14), 15) . '| ' .
      str_pad($field_array['html_status'], 6) . '| ' .
      str_pad($field_array['content_size'], 8) . '| ' .
      str_pad($field_array['wid_before_test'], 12) . '| ' .
      str_pad($field_array['wid_after_test'], 12) . '| ' .
      str_pad($field_array['delta_watchdog'], 6) . '| ' .
      str_pad($field_array['delta_php'], 6) . '| ' .
      str_pad($field_array['delta_emergency'], 6) . '| ' .
      str_pad($field_array['delta_alert'], 6) . '| ' .
      str_pad($field_array['delta_critical'], 6) . '| ' .
      str_pad($field_array['delta_error'], 6) . '| ' .
      str_pad($field_array['delta_warning'], 6) . '| ' .
      str_pad($field_array['delta_notice'], 6) . '| ' .
      str_pad($field_array['delta_info'], 6) . '| ' .
      str_pad($field_array['delta_debug'], 6) . '| ' .
      str_pad(date('Y/m/d H:i:s', $field_array['timestamp']), 20) . "|\r\n";

  }

}
