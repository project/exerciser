<?php

namespace Drupal\exerciser\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for page example routes.
 */
class ExerciserController extends ControllerBase {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'exerciser';
  }

  /**
   * Content Type Distribution Report.
   */
  public function distribution() {

    $query = "SELECT `type` AS `type`, COUNT(`type`) AS `num_pages`, (COUNT(`type`) * 100 / (SELECT COUNT(*) FROM {node})) as `percentage` FROM {node} GROUP BY `type`";
    $result = $this->database->query($query);

    $header = ['Content Type of Pages', 'Number of Pages', 'Percentage'];
    $rows = [];

    foreach ($result as $record) {
      $rows[] = [$record->type, $record->num_pages, $record->percentage . ' %'];
    }

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $form;

  }

}
